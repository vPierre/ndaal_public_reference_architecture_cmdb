#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2023, 2024
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 12.x; macOS Sonoma x86 architecture
# Tested on: Debian 12.x; macOS Sonoma x86 architecture

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}
# KISS Colors (v1): https://github.com/ppo/bash-colors/blob/master/kiss-colors.sh


readonly reset="\e[0m"       # Uppercase = bold.
readonly black="\e[0;30m";   readonly BLACK="\e[1;30m"
readonly red="\e[0;31m";     readonly RED="\e[1;31m"
readonly green="\e[0;32m";   readonly GREEN="\e[1;32m"
readonly yellow="\e[0;33m";  readonly YELLOW="\e[1;33m"
readonly blue="\e[0;34m";    readonly BLUE="\e[1;34m"
readonly magenta="\e[0;35m"; readonly MAGENTA="\e[1;35m"
readonly cyan="\e[0;36m";    readonly CYAN="\e[1;36m"
readonly white="\e[0;37m";   readonly WHITE="\e[1;37m"
readonly NOFORMAT=""

#function heading() { printf "${WHITE}${1}${reset}\n"; }
#function warning() { printf "${magenta}Warning: ${1}${reset}\n"; }
#function error() { printf "${red}ERROR: ${1}${reset}\n"; }
#function success() { printf "${green}${1:-Successfully done.}${reset}\n"; }
#function abort() { printf "${RED}Abort. ${1}${reset}\n\n"; exit; }

echo "${magenta}"
echo "${MAGENTA}"
echo "sourcing KISS Colors is finished"
