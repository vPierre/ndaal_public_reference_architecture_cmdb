#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2023, 2024
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 12.x; macOS Sonoma x86 architecture
# Tested on: Debian 12.x; macOS Sonoma x86 architecture

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

usage() {
  cat <<EOF

This Script prepares the repo for check in.

The repo is:

https://gitlab.com/vPierre/ndaal_public_reference_architecture_cmdb

EOF
  exit
}

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

# Load configuration from deployment.cfg
configfile="deployment.cfg"
echo "Reading configuration from: ${configfile}"

echo "Sourcing ${configfile}"
# shellcheck source=/dev/null
source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/${configfile}"

echo "Sourcing tools/kiss-colors.sh"
# shellcheck source=/dev/null
source "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/tools/kiss-colors.sh"

# Under Linux you use `home` under macOS `Users`
echo "Home directory: ${HOMEDIR}"

# Your user! In which context it SHOULD run
echo "User script: ${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
echo "Current date: ${DIRDATE}"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/repos/ndaal_public_reference_architecture_cmdb/"
echo "Destination directory: ${DESTINATION}"

echo "Check if some tools are available"
command -v "git" || { echo "git not found. Please install it."; exit 1; }
command -v "wget" || { echo "wget not found. Please install it."; exit 1; }
command -v "blockdiag" || { echo "wget not found. Please install it."; exit 1; }

# Execute the DNS check script
#./check_dns_servers.sh

# Check the exit code of the DNS check script
dns_check_exit_code=$?

# If the DNS check script failed (non-zero exit code), exit the calling script
if [ ${dns_check_exit_code} -ne "0" ]; then
    echo "DNS check script failed. Exiting."
    exit ${dns_check_exit_code}
fi

# Continue with your other commands if the DNS check script succeeded

git config --list
"${DESTINATION}tools/git_credential.sh"
"${DESTINATION}tools/git_config.sh"
git config --list

DIRECTORY="${DESTINATION}.build"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}.config"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}dep"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}documentation"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    #touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}documentation/collected"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    #touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}example"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

cd "${DESTINATION}" || exit
echo "Changed to the desired directory: ${DESTINATION}"

DIRECTORY="${DESTINATION}res"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}test"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
fi

DIRECTORY="${DESTINATION}tools"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if ${DESTINATION} doesn't exist.
    mkdir -p -v "${DIRECTORY}"
    touch "${DIRECTORY}/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY} is created"
    mkdir -p -v "${DIRECTORY/}Linux"
    touch "${DIRECTORY}/Linux/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY}/Linux is created"
    mkdir -p -v "${DIRECTORY}/Windows"
    touch "${DIRECTORY}/Windows/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY}/Windows is created"
    mkdir -p -v "${DIRECTORY}/macOS"
    touch "${DIRECTORY}/macOS/placeholder.txt"
    echo -e "${YELLOW} the directory ${DIRECTORY}/macOS is created"
fi

echo -e "${reset}"

Function_Git_Pull_Repos () {
    (
    echo " "
    echo "Git Pull from Repos"
    echo " "

    #git branch master || true
    branch_name="vPierre"
    git branch "${branch_name}" || true
    git checkout "${branch_name}" || true
    
    git config pull.rebase false
    git pull --verbose
    )
}

Function_Git_Push_Repos () {
    echo " "
    echo "Git Push to Repos"
    echo " "
    git add --verbose -A || true
    git commit -m "update collect test data" || true
    #git commit -m "update # nosemgrep: ifs-tampering" || true
    git push origin HEAD:master || true
    git push --verbose --force || true
}

Function_Create_Default_Information_for_Repos () {
    echo "update structure.txt"
    tree > ./"structure.txt" || exit
    cat ./"structure.txt" || exit
    echo "update content_summary.txt"
    tokei ---sort code > "content_summary.txt" || exit
    cat ./"content_summary.txt" || exit
    echo "update content_long_list.txt"
    tokei ---sort code --files > "content_long_list.txt" || exit
    cat ./"content_long_list.txt" || exit
}

Function_Create_Default_Files_for_Repos () {
    echo "Create Default Files for Repos"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://ndaal.eu/.well-known/security.txt" || true
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate "https://gitlab.com/ndaal_open_source/ndaal_public_git_ignore/-/blob/453358018843aae46da5be681a4635d24f7637c4/.gitignore" || true

    echo "Create CODE_OF_CONDUCT Files for Repos"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT.txt" "https://www.contributor-covenant.org/version/2/1/code_of_conduct/code_of_conduct.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT.md" "https://www.contributor-covenant.org/version/2/1/code_of_conduct/code_of_conduct.md"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT_DE.txt" "https://www.contributor-covenant.org/de/version/2/1/code_of_conduct/code_of_conduct.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "CODE_OF_CONDUCT_DE.md" "https://www.contributor-covenant.org/de/version/2/1/code_of_conduct/code_of_conduct.md"

    echo "Create robots.txt for Repos"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "robots.txt" "https://gitlab.com/vPierre/ndaal_public_robots_txt/-/blob/main/robots.txt"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate --output-document "${DESTINATION}documentation/robots.txt" "https://gitlab.com/vPierre/ndaal_public_robots_txt/-/blob/main/robots.txt"

    echo "Create Vulnerability Disclosure Policy.md for Repos"
    wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate  --output-document "Vulnerability_Disclosure_Policy.md" "https://gitlab.com/vPierre/ndaal_public_vulnerability_disclosure_policy/-/raw/main/Vulnerability_Disclosure_Policy.md?ref_type=heads"
}

Function_Remove_Unwished_Files () {
    echo " "
    echo "Remove Unwished Files"
    echo "Currently, files with these extensions are considered temporary files"
    echo "thumbs.db", ".bak", "~", ".tmp", ".temp", ".ds_store", ".crdownload", ".part", ".cache", ".dmp", ".download", ".partial"]
    echo " "
    echo "remove .DS_Store recursive"
    find . -name ".DS_Store" -exec rm -rf {} \; || true
    echo "remove thumbs.db recursive"
    find . -name "thumbs.db" -exec rm -rf {} \; || true
    echo "remove Thumbs.db recursive"
    find . -name "Thumbs.db" -exec rm -rf {} \; || true
    echo "remove .crdownload recursive"
    find . -name ".crdownload" -exec rm -rf {} \; || true
    echo "remove .download recursive"
    find . -name ".download" -exec rm -rf {} \; || true
    echo "remove .partial recursive"
    find . -name ".partial" -exec rm -rf {} \; || true
    echo "remove .cache recursive"
    find . -name ".cache" -exec rm -rf {} \; || true
    echo "remove .dmp recursive"
    find . -name ".dmp" -exec rm -rf {} \; || true
    echo "remove .tmp recursive"
    find . -name ".tmp" -exec rm -rf {} \; || true
    echo "remove .temp recursive"
    find . -name ".temp" -exec rm -rf {} \; || true
    echo "remove .bak recursive"
    find . -name ".bak" -exec rm -rf {} \; || true
    echo "remove .log recursive"
    find . -name ".log" -exec rm -rf {} \; || true
    rg -F 'thumbs.db' --files | xargs rm -rf {} \; || true
    rg -F 'Thumbs.db' --files | xargs rm -rf {} \; || true
    rg -F '.DS_Store' --files | xargs rm -rf {} \; || true
}

Function_Create_Checksums_for_Scripts () {
    echo "Create checksums files "
    echo "${DESTINATION}"
    # cargo install sha3sum
    find . -name ".sha-3-512" -exec rm -rf {} \; || true
    find . -name ".sha512" -exec rm -rf {} \; || true
    find "${DESTINATION}" -type f -name "*.sh" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
    find "${DESTINATION}" -type f -name "*.sh" -exec sh -c "twoxhash {} | awk '{print \$1}' | tee {}.twoxhash" \;
    find "${DESTINATION}" -type f -name "*.sh" -exec sh -c "k12sum {} | awk '{print \$1}' | tee {}.k12" \;
    #find "${DESTINATION}" -type f -name '*.sh' -exec sh -c 'i="${1}"; sha512sum "${i}" "${i%.sh}.sha512"' shell {} \;
    #find "${DESTINATION}" -type f -name '*.sh' -exec sh -c 'i="${1}"; checksum=$(sha512sum "${i}" | awk '\''{print $1}'\''); echo "${checksum} $(basename "${i}" .sh)" > "$(dirname "${i}")/$(basename "${i}" .sh).sha512"' shell {} \;

    #exit 0
}

Function_Create_Checksums_for_Markdown () {
    echo "Create checksums files "
    echo "${DESTINATION}"
    find "${DESTINATION}" -type f -name "*.md" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
}

Function_Create_Checksums_for_reStructuredText () {
    echo "Create checksums files "
    echo "${DESTINATION}"
    find "${DESTINATION}" -type f -name "*.rst" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
}

blockdiag Reference_Architecture_CMDB.diag
blockdiag Reference_Architecture_CMDB.diag -Tsvg

cd "${DESTINATION}" || exit
echo "Changed to the desired directory: ${DESTINATION}"

Function_Create_Default_Files_for_Repos
Function_Remove_Unwished_Files
Function_Create_Checksums_for_Markdown
Function_Create_Checksums_for_reStructuredText
Function_Create_Checksums_for_Scripts
git repack -a -d -f --depth=250 --window=250
Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

script_name1="$(basename "${0}")"
echo "script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
echo "script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
